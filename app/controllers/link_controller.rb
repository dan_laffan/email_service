class LinkController < ApplicationController
  def show
    message_user = current_user.message_users.find_by(uuid: params[:id])
    if message_user
      redirect_to email_url(message_user.id)
    else
      flash[:error] = I18n.t('controllers.link.show.error')
      redirect_to emails_url
    end
  end
end

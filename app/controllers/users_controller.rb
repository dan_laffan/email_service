class UsersController < ApplicationController
  before_action :get_singleton, only: %i(show edit update destroy)

  def index
    @users = User.all_in_order
  end

  def show
  end

  def new
    @user = User.new
  end

  def edit
  end

  def create
    @user = User.new(user_params)
    if @user.save
      flash[:success] = I18n.t('controllers.users.create.success')
      redirect_to @user
    else
      flash.now[:error] = I18n.t('controllers.users.create.error')
      render :new
    end
  end

  def update
    if @user.update_attributes(user_params)
      flash[:success] = I18n.t('controllers.users.update.success')
      redirect_to @user
    else
      flash.now[:error] = I18n.t('controllers.users.update.error')
      render :edit
    end
  end

  def destroy
    if @user.destroy
      flash[:success] = I18n.t('controllers.users.destroy.success')
      redirect_to new_user_session_url
    else
      flash[:error] = I18n.t('controllers.users.destroy.error')
      redirect_to users_url
    end
  end

  protected

  def get_singleton
    if current_user&.admin?
      @user = User.find(params[:id])
    else
      @user = current_user
    end
  end

  def user_params
    params.require(:user).permit(:email, :first_name, :last_name, :password)
  end
end

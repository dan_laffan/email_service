class EmailsController < ApplicationController
  before_action :set_singleton, only: %i(show destroy)

  # GET /messages
  def index
    @sent_message_users = current_user.message_users.all_sent.all_in_order
    @inbox_message_users = current_user.message_users.all_inbox.all_in_order
  end

  # GET /messages/1
  def show
    @message_user.update_attribute(:viewed, true)
  end

  # DELETE /messages/1
  def destroy
    if @message_user.destroy
      flash[:success] = I18n.t('controllers.emails.destroy.success')
    else
      flash[:error] = I18n.t('controllers.emails.destroy.error')
    end
    redirect_to emails_url
  end

  private

  def set_singleton
    @message_user = current_user.message_users.find(params[:id])
  end
end

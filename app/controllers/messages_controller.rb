class MessagesController < ApplicationController
  before_action :set_singleton, only: %i(edit update destroy)
  before_action :set_users

  # GET /messages/new
  def new
    @message = current_user.sent_messages.new
    @message.message_users.build(user_id: User.find_by(email: params[:to])&.id)
  end

  # GET /messages/1/edit
  def edit
  end

  # POST /messages
  def create
    @message = current_user.sent_messages.new(message_params)
    if @message.save
      flash[:success] = I18n.t('controllers.emails.create.success')
      redirect_to emails_url
    else
      flash[:error] = I18n.t('controllers.emails.create.error')
      render :new
    end
  end

  # PATCH/PUT /messages/1
  def update
    if @message.update(message_params)
      flash[:success] = I18n.t('controllers.emails.update.success')
      redirect_to emails_url
    else
      flash[:error] = I18n.t('controllers.emails.update.error')
      render :edit
    end
  end

  protected

  def message_params
    params.require(:message).permit(:subject_line, :body_content, message_users_attributes: [:user_id])
  end

  def set_singleton
    @message = current_user.sent_messages.find(params[:id])
  end

  def set_users
    @users = User.where.not(id: current_user.id).all_in_order
  end
end

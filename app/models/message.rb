class Message < ActiveRecord::Base
  # relationships
  belongs_to :creator, class_name: 'User', foreign_key: :created_by
  has_many :message_users, inverse_of: :message
  accepts_nested_attributes_for :message_users

  # validations
  validates :creator, presence: true
  validates :subject_line, presence: true, length: { maximum: 255 }
  validates :body_content, presence: true, length: { maximum: 65_535 }

  # callbacks
  after_create :create_owner_message_user_record

  protected

  def create_owner_message_user_record
    MessageUser.create(user_id: created_by, message_id: id)
  end
end

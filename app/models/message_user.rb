class MessageUser < ActiveRecord::Base
  # relationships
  belongs_to :message, inverse_of: :message_users
  belongs_to :user

  # validation
  validates :message, presence: true
  validates :user_id, presence: true

  # callbacks
  before_validation :initialize_some_fields, on: :create
  after_create :enqueue_email

  # scopes
  scope :all_in_order, -> { order(user_id: :asc, viewed: :desc, message_id: :desc) }
  scope :all_viewed, -> { where(viewed: true) }
  scope :all_not_viewed, -> { where(viewed: false) }
  scope :all_inbox, -> { includes(:message).where('messages.created_by != message_users.user_id').references(:message) }
  scope :all_sent, -> { includes(:message).where('messages.created_by = message_users.user_id').references(:message) }

  protected

  def enqueue_email
    SendNotificationsJob.set(wait: 5.seconds).perform_later(id) unless user_id == message.created_by
  end

  def initialize_some_fields
    self.uuid = SecureRandom.urlsafe_base64(20)
    self.viewed = false
    true
  end
end

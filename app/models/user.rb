class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # relationships
  has_many :inbox_messages, through: :message_users, source: :message
  has_many :message_users
  has_many :sent_messages, foreign_key: :created_by, class_name: 'Message'

  # validation
  validates :first_name, presence: true
  validates :last_name, presence: true

  # scopes
  scope :all_in_order, -> { order(:last_name, :first_name) }

  # instance methods
  def admin?
    false # TODO: implement hierarchical authorisations later
  end

  def full_name
    (first_name + ' ' + last_name).titleize
  end

  def full_name_and_email
    "#{full_name} (#{email})"
  end
end

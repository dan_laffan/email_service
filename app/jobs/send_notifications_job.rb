class SendNotificationsJob < ActiveJob::Base
  queue_as :default

  def perform(message_user_id)
    message_user = MessageUser.find(message_user_id)
    email = MailGun.new(build_payload(message_user))
    if email.valid?
      email.send_now unless Rails.env.test?
      message_user.update_attribute(:alert_sent_at, Time.now)
    else
      Rails.logger.warn "SendNotificationsJob#perform failed to send MessageUser##{message_user_id}. " \
                        "Errors: #{email&.errors}."
      true
    end
  end

  protected

  def build_payload(message_user)
    { recipient_name: message_user.user.full_name, recipient_email: message_user.user.email,
      sender_name: message_user.message.creator.full_name,
      sender_email: message_user.message.creator.email,
      subject: I18n.t('jobs.send_notifications.subject'),
      message: message_content(message_user.message.creator.full_name,
                               message_user.message.subject_line, message_user.uuid) }
  end

  def message_content(sender_name, subject, uuid)
    [
      I18n.t('jobs.send_notifications.greeting'), '',
      I18n.t('jobs.send_notifications.first_paragraph'), '',
      I18n.t('jobs.send_notifications.from', name: sender_name),
      I18n.t('jobs.send_notifications.subject_line', text: subject),
      (I18n.t('jobs.send_notifications.linkback') + " #{configatron.app.url}/link/#{uuid}"),
      '', I18n.t('jobs.send_notifications.last_paragraph'),
      '', '', I18n.t('jobs.send_notifications.signature')
    ].join("\n")
  end
end

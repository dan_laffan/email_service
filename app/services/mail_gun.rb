require 'rest-client'

class MailGun
  include ActiveModel::Model

  attr_accessor :recipient_name, :recipient_email, :sender_name, :sender_email, :subject, :message

  # validation
  validates :recipient_name, presence: true
  validates :recipient_email, presence: true
  validates :sender_name, presence: true
  validates :sender_email, presence: true
  validates :subject, presence: true
  validates :message, presence: true

  def send_now
    RestClient.post caller_url,
                    from: "Dan's Codetest App <postmaster@#{configatron.mail_gun.api_key}.mailgun.org>",
                    to: "#{recipient_name} <#{recipient_email}>",
                    cc: "#{sender_name} <#{sender_email}>", # could be removed later
                    subject: subject,
                    text: message
  end

  protected

  def caller_url
    "https://api:key-#{configatron.mail_gun.api_secret}" \
    "@api.mailgun.net/v3/#{configatron.mail_gun.api_key}.mailgun.org/messages"
  end
end

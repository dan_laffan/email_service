// JS for the MessagesController

$(document).on('ready', function() {

  // methods that add a new user
  function addUser() {
    // Set up some variables
    var messageUsersObject = $('#message-users'),
        firstGroup = messageUsersObject.find('.form-group').first(),
        newGroup = firstGroup.clone(),
        newGroupSelect = newGroup.find('select'),
        timeStamp = (new Date).getTime();

    // rename stuff inside the newGroup
    newGroup.find('label').attr('for', 'message_message_users_attributes_' + timeStamp + '_user_id');
    newGroupSelect.attr('name', 'message[message_users_attributes][' + timeStamp + '][user_id]');
    newGroupSelect.attr('id', 'message[message_message_users_attributes_' + timeStamp + '_user_id');
    newGroupSelect.val(null);

    // Add the updated newGroup to the UI
    messageUsersObject.append(newGroup);
  }

  function removeUser(clickedThing) {
    var formGroupObject = clickedThing.closest('.form-group'),
        messageUsersObject = $('#message-users'),
        groupCount = messageUsersObject.find('.form-group').length;

    if (groupCount > 1) {
      formGroupObject.remove();
    } else {
      alert("Sorry, you can't remove the last one");
    }
  }

  $('#page-messages-new, #page-messages-create')
    // Add user button
    .on('click', '.user-add-button', function() {
      addUser();
    })
    // Remove User button
    .on('click', '.user-remove-button', function() {
      removeUser(this);
    });
});

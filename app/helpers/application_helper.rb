module ApplicationHelper
  def message_viewed_or_not(viewed)
    if viewed
      content_tag(:span, t('views.general.viewed'), class: 'label label-success')
    else
      content_tag(:span, t('views.general.not_viewed'), class: 'label label-info')
    end
  end
end

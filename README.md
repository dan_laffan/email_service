# EMail Service

A simple demo Rails app to allow registered users to send and receive email messages to each other.  These are the requirements;

- User management (basically CRUD users)
- User log in (use devise for this)
- An email interface that:
    - Lists a user's emails
    - Allows the user to compose an email containing:
        - A subject
        - A Body
    - A list of other users in the system (many-to-many) that the user can select to send the email to.
        * Mention of many-to-many implies a base message object, and multiple message-recipient objects.
- Send the email using MailGun (send the email through MailGun's free service)
    - Implies deferred sending of emails via a background processing engine.  The app uses Sidekiq and Redis via ActiveJob.

## Implementation overview

To deliver the core requirements, the app uses the following models:

* message
    * This model contains the author ID, the subject, and the body text
* message_user
    * This model represents the link between the message and each recipient
    * The models contains 
        * the recipient user ID, 
        * the date & time the outbound alert email was sent, 
        * a boolean to track that the message was read,
        * a UUID to enable recipients to link back to the site to view the message.

## System dependencies

The following items need to be installed on your machine to get this app running:

* Ruby 2.3.1
    ```
    rvm use 2.3.1
    ```
* The latest bundler gem
    ``` 
    gem install bundler
    ```
* Redis
    ```
    brew install redis
    ```
* postgresql server
    ```
    brew install postgresql
    ```
* Environment variables (see the section on Services below).

## Configuration

Settings that change between environment are handled using configatron.  See the [gem's documentation](https://github.com/markbates/configatron) for further information.  

Items that are managed by configatron are initialized in `config/configatron/default.rb` and `./<ENVIRONMENT>.rb` for environment-specific values.

## Database creation & initialization

The app uses Postgres.  To set up a fresh development and test database for this app:
```sh
RAILS_ENV=development bundle exec rake db:create db:migrate
RAILS_ENV=test bundle exec rake db:create db:migrate
```

If you would like to have some seed data in your development environment, load the `seeds.rb` file:
```sh
RAILS_ENV=development bundle exec rake db:seed
```

## How to run the app

You'll need the following sequence (or similar) to get the app up and running:
```sh
redis-server &
bundle exec sidekiq &
```
You then have a choice of using Webrick as your web server:
```sh 
bundle exec rails s
```
or Unicorn:
```sh 
unicorn -l lvh.me:3000
```

## How to run the tests

The app's tests are written using RSpec.  Basic unit tests exist in the model layer, and minimalist tests in the controller layer.  Controller tests also trigger the rendering of views in-memory, so exceptions that would be raised in the view layer will cause controller tests to fail.

To run the tests:
```sh 
rspec
# runs all tests
```
```sh 
rspec controllers/
# runs all the controller tests
```

## Services 

### Job Queues

One job queue exists `SendNotificationJob`, which accepts a MessageUser ID, and when executed, calls the `MailGun` class to send an email.

Jobs are managed using `ActiveJob` with a `Sidekiq` back-end (which in turn uses `Redis`).

## Services

The `MailGun` class (in `app/services/mail_gun.rb`) handles sending emails via [MailGun](https://mailgun.com).  You will need to set your API key and secret token as environment variables; for example: 

```sh 
# ~/.profile

export MAIL_GUN_API_KEY=<your_key>
export MAIL_GUN_API_SECRET=<your_secret>
```

You can obtain these keys using a free account on [MailGun's website](https://mailgun.com).

## Deployment instructions - TBD

Capistrano is loaded in the gem file but nothing other than that is done to enable this app to be deployed.

## License

Copyright © Daniel M. Laffan, 2016.  All rights reserved.

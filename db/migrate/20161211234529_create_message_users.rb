class CreateMessageUsers < ActiveRecord::Migration
  def change
    create_table :message_users do |t|
      t.integer :message_id, index: true
      t.integer :user_id, index: true
      t.boolean :viewed, default: false
      t.datetime :alert_sent_at
      t.string :uuid

      t.timestamps null: false
    end
  end
end

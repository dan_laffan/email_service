class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.integer :created_by, index: true
      t.string :subject_line
      t.text :body_content

      t.timestamps null: false
    end
  end
end

# preliminary stuff
def ask(question, default_value)
  print question
  print " (defaults to '#{default_value}'): " if default_value
  input = STDIN.gets.chomp
  input.blank? ? default_value : input
end

def heading(msg)
  puts msg
  puts '=' * msg.length
  puts
end

def seed_specific_users
  # default values supplied for convenience
  users = [
    {email: 'dan.laffan@mac.com', first_name: 'Dan', last_name: 'Laffan', password: '123123123'},
    {email: 'dan.laffan.test@gmail.com', first_name: 'Dan', last_name: 'Laffan-Test', password: '123123123'},
    {email: 'dan.laffan.fca@gmail.com', first_name: 'Dan', last_name: 'Laffan-FCA', password: '123123123'},
  ]

  3.times do |index|
    puts "For user ##{index + 1}:"
    users[index][:email]      = ask('- enter the email address: ', users[index][:email])
    users[index][:first_name] = ask('- enter the first name:    ', users[index][:first_name])
    users[index][:last_name]  = ask('- enter the last name:     ', users[index][:last_name])
  end

  puts
  print 'OK: creating those users now: '
  users.each do |user_details|
    User.create!(user_details)
    print '.'
  end
  puts ' DONE'
  puts
end

def seed_dummy_users
  print 'Creating three dummy users: '
  3.times do |index|
    User.create!(email: "dummy-#{index + 1}@example.com", first_name: 'Dummy', last_name: "User-#{index + 1}",
                 password: '123123123')
    print '.'
  end
  puts ' DONE'
end

# User seeds
def seed_users
  system 'clear'
  heading('User Seeding')

  puts 'You need to create three users with real email addresses that have been white-listed with MailGun.com.'
  puts "You'll also get some dummy @example.com accounts to play with. MailGun won't deliver to these addresses."

  seed_specific_users
  seed_dummy_users

  puts
  puts 'Seeding has completed successfully.'
end

# call the main method
seed_users

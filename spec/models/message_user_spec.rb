require 'rails_helper'

RSpec.describe MessageUser, type: :model do
  context 'relationships' do
    it { is_expected.to belong_to(:message) }
    it { is_expected.to belong_to(:user) }
  end

  context 'validations' do
    it { is_expected.to validate_presence_of(:message) }
    it { is_expected.to validate_presence_of(:user_id) }
  end

  context 'callbacks' do
    it { is_expected.to callback(:enqueue_email).after(:create) }
    it { is_expected.to callback(:initialize_some_fields).before(:validation).on(:create) }
  end

  context 'scopes' do
    it { expect(MessageUser).to respond_to(:all_in_order) }
    it { expect(MessageUser).to respond_to(:all_viewed) }
    it { expect(MessageUser).to respond_to(:all_not_viewed) }
    it { expect(MessageUser).to respond_to(:all_inbox) }
    it { expect(MessageUser).to respond_to(:all_sent) }
  end
end

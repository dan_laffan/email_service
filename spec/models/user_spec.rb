require 'rails_helper'

RSpec.describe User, type: :model do
  context 'relationships' do
    it { is_expected.to have_many(:inbox_messages) }
    it { is_expected.to have_many(:message_users) }
    it { is_expected.to have_many(:sent_messages) }
  end

  context 'validations' do
    it { is_expected.to validate_presence_of(:first_name) }
    it { is_expected.to validate_presence_of(:last_name) }
    it { is_expected.to validate_presence_of(:email) }
  end

  context 'scopes' do
    it { expect(User).to respond_to(:all_in_order) }
  end

  context 'instance methods' do
    it { is_expected.to respond_to(:admin?) }
    it 'test the functioning of admin? later'
    describe 'full_name' do
      it { is_expected.to respond_to(:full_name) }

      subject { FactoryGirl.create(:user, first_name: 'jo', last_name: 'smith') }
      it { expect(subject.full_name).to eq('Jo Smith') }
    end

    it { is_expected.to respond_to(:full_name_and_email) }
  end
end

require 'rails_helper'

RSpec.describe Message, type: :model do
  context 'relationships' do
    it { is_expected.to belong_to(:creator) }
    it { is_expected.to have_many(:message_users) }
    it { is_expected.to accept_nested_attributes_for(:message_users) }
  end

  context 'validations' do
    it { is_expected.to validate_presence_of(:creator) }

    it { is_expected.to validate_presence_of(:subject_line) }
    it { is_expected.to validate_length_of(:subject_line).is_at_most(255) }

    it { is_expected.to validate_presence_of(:body_content) }
    it { is_expected.to validate_length_of(:body_content).is_at_most(65_535) }
  end

  context 'callbacks' do
    it { is_expected.to callback(:create_owner_message_user_record).after(:create) }
  end
end

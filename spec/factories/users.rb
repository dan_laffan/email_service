FactoryGirl.define do
  factory :user do
    first_name            'John'
    sequence(:last_name)  { |x| "Lastname-#{x}" }
    sequence(:email)      { |x| "testuser-#{x}@example.com"}
    password              '123123123'
  end
end

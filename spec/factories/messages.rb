FactoryGirl.define do
  factory :message do
    created_by   1
    subject_line 'MyString'
    body_content 'MyText'
  end
end

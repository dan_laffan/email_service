require 'rails_helper'

RSpec.describe EmailsController, type: :controller do
  include Devise::Test::ControllerHelpers
  let!(:user_1) { FactoryGirl.create(:user) }
  let!(:user_2) { FactoryGirl.create(:user) }
  let!(:message_1) do
    FactoryGirl.create(:message, created_by: user_1.id,
                       message_users_attributes: [ { user_id: user_2.id } ])
  end
  let!(:message_2) do
    FactoryGirl.create(:message, created_by: user_1.id,
                        message_users_attributes: [ { user_id: user_2.id } ])
  end
  let!(:message_3) do
    FactoryGirl.create(:message, created_by: user_2.id,
                        message_users_attributes: [ { user_id: user_1.id } ])
  end
  let!(:message_attributes) { FactoryGirl.attributes_for(:message) }

  render_views

  before(:each) do
    sign_in(user_1)
  end

  describe 'GET #index' do
    it 'returns http success' do
      get :index
      expect(response).to have_http_status(:success)
    end

    it 'assigns @messages with two messages' do
      get :index
      expect(assigns(:sent_message_users).count).to eq(2)
      expect(assigns(:sent_message_users).first.class).to eq(MessageUser)
      expect(assigns(:sent_message_users).first.user_id).to eq(user_1.id)
      expect(assigns(:sent_message_users).last.user_id).to eq(user_1.id)
      expect(assigns(:inbox_message_users).count).to eq(1)
      expect(assigns(:inbox_message_users).first.user_id).to eq(user_1.id)
      expect(assigns(:inbox_message_users).first.message.created_by).to eq(user_2.id)
      expect(assigns(:inbox_message_users).first.class).to eq(MessageUser)
    end
  end

  describe 'GET #show' do
    it 'returns http success' do
      get :show, id: user_1.message_users.first.id
      expect(response).to have_http_status(:success)
    end

    it 'assigns @message with message_1' do
      get :show, id: user_1.message_users.first.id
      expect(assigns(:message_user).id).to eq(user_1.message_users.first.id)
    end
  end

  describe 'DELETE #destroy' do
    it 'returns http success' do
      delete :destroy, id: user_1.message_users.last.id
      expect(response).to redirect_to(emails_url)
    end
  end
end

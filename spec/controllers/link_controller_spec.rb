require 'rails_helper'

RSpec.describe LinkController, type: :controller do
  include Devise::Test::ControllerHelpers

  let!(:user_1) { FactoryGirl.create(:user) }
  let!(:user_2) { FactoryGirl.create(:user) }
  let!(:message_1) do
    FactoryGirl.create(:message, created_by: user_2.id,
                       message_users_attributes: [ { user_id: user_1.id } ])
  end

  render_views

  before(:each) do
    sign_in(user_1)
  end

  describe 'GET #show' do
    it 'returns http redirect to emails#show (good token)' do
      get :show, id: message_1.message_users.first.uuid
      expect(response).to redirect_to(email_url(message_1.message_users.first.id))
    end

    it 'returns http redirect to /emails (bad token)' do
      get :show, id: 'abc123'
      expect(response).to redirect_to(emails_url)
    end
  end
end

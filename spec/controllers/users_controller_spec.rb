require 'rails_helper'

RSpec.describe UsersController, type: :controller do
  include Devise::Test::ControllerHelpers

  let!(:user_1) { FactoryGirl.create(:user) }
  let!(:user_2) { FactoryGirl.create(:user) }
  let!(:user_attributes) { FactoryGirl.attributes_for(:user) }

  render_views

  before(:each) do
    sign_in(user_1)
  end

  describe 'GET #index' do
    it 'returns http success' do
      get :index
      expect(response).to have_http_status(:success)
    end

    it 'assigns @users with two users' do
      get :index
      expect(assigns(:users).count).to eq(2)
      expect(assigns(:users).first.class).to eq(User)
    end
  end

  describe 'GET #show' do
    it 'returns http success' do
      get :show, id: user_1.id
      expect(response).to have_http_status(:success)
    end

    it 'assigns @user with user_1' do
      get :show, id: user_1.id
      expect(assigns(:user).id).to eq(user_1.id)
    end
  end

  describe 'GET #new' do
    it 'returns http success' do
      get :new
      expect(response).to have_http_status(:success)
      expect(assigns(:user).class).to eq(User)
    end
  end

  describe 'GET #edit' do
    it 'returns http success' do
      get :edit, id: user_1.id
      expect(response).to have_http_status(:success)
    end
  end

  describe 'POST #create' do
    it 'returns http success' do
      post :create, user: user_attributes
      expect(response).to redirect_to(user_path(assigns(:user).id))
    end
  end

  describe 'PUT #update' do
    it 'returns http success' do
      put :update, id: user_1.id, user: user_attributes
      expect(response).to redirect_to(user_path(user_1.id))
    end
  end

  describe 'DELETE #destroy' do
    it 'returns http success' do
      delete :destroy, id: user_1.id
      expect(response).to redirect_to(new_user_session_url)
    end
  end
end

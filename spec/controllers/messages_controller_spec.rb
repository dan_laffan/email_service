require 'rails_helper'

RSpec.describe MessagesController, type: :controller do
  include Devise::Test::ControllerHelpers
  let!(:user_1) { FactoryGirl.create(:user) }
  let!(:user_2) { FactoryGirl.create(:user) }
  let!(:message_1) { FactoryGirl.create(:message, created_by: user_1.id) }
  let!(:message_2) { FactoryGirl.create(:message, created_by: user_1.id) }
  let!(:message_3) { FactoryGirl.create(:message, created_by: user_2.id) }
  let!(:message_attributes) { FactoryGirl.attributes_for(:message) }

  render_views

  before(:each) do
    sign_in(user_1)
  end

  describe 'GET #new' do
    it 'returns http success' do
      get :new
      expect(response).to have_http_status(:success)
      expect(assigns(:message).class).to eq(Message)
    end
  end

  describe 'GET #edit' do
    it 'returns http success' do
      get :edit, id: message_1.id
      expect(response).to have_http_status(:success)
    end
  end

  describe 'POST #create' do
    it 'returns http success' do
      post :create, message: message_attributes
      expect(response).to redirect_to(emails_url)
    end
  end

  describe 'PUT #update' do
    it 'returns http success' do
      put :update, id: message_1.id, message: message_attributes
      expect(response).to redirect_to(emails_path)
    end
  end
end

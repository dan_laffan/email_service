# frozen_string_literal: false

require 'spec_helper'

describe UsersController, type: :routing do
  describe 'routing' do
    it 'should route to #index' do
      expect(get: '/users').to route_to('users#index')
    end

    it 'should route to #show' do
      expect(get: '/users/1').to route_to('users#show', id: '1')
    end

    it 'should route to #new' do
      expect(get: '/users/new').to route_to('users#new')
    end

    it 'should route to #edit' do
      expect(get: '/users/1/edit').to route_to('users#edit', id: '1')
    end

    it 'should route to #create' do
      expect(post: '/users').to route_to('devise/registrations#create')
    end

    it 'should route to #update' do
      expect(put: '/users/1').to route_to('users#update', id: '1')
    end

    it 'should route to #destroy' do
      expect(delete: '/users/1').to route_to('users#destroy', id: '1')
    end
  end
end

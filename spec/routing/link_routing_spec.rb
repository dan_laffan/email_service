require 'rails_helper'

RSpec.describe LinkController, type: :routing do
  describe 'routing' do
    it 'routes to #show' do
      expect(get: '/link/abc123').to route_to('link#show', id: 'abc123')
    end
  end
end

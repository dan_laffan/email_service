require 'rails_helper'

RSpec.describe EmailsController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/emails').to route_to('emails#index')
    end

    it 'routes to #show' do
      expect(get: '/emails/1').to route_to('emails#show', id: '1')
    end

    it 'routes to #destroy' do
      expect(delete: '/emails/1').to route_to('emails#destroy', id: '1')
    end
  end
end
